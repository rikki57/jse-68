package ru.nlmk.study.jse68.repository;

import org.springframework.data.repository.CrudRepository;
import ru.nlmk.study.jse68.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
