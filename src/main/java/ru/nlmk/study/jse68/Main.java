package ru.nlmk.study.jse68;

import org.springframework.context.ApplicationContext;
import ru.nlmk.study.jse68.model.CreditCard;
import ru.nlmk.study.jse68.model.User;
import ru.nlmk.study.jse68.repository.UserRepository;

import java.time.Instant;
import java.util.ArrayList;

public class Main {

    private static void fillDb(ApplicationContext context) {
        User user = User.builder()
                .age(23)
                .startDate(Instant.now())
                .endDate(Instant.now())
                .name("Jack")
                .surname("Daniels")
                .creditCards(new ArrayList<>())
                .build();
        CreditCard creditCard = CreditCard.builder()
                .expMonth("03")
                .expYear("21")
                .cardNumber("343 34343  343434 ")
                .user(user)
                .build();
        CreditCard creditCard2 = CreditCard.builder()
                .expMonth("04")
                .expYear("22")
                .cardNumber("3434 5523 4434 2232")
                .user(user)
                .build();
        user.getCreditCards().add(creditCard);
        user.getCreditCards().add(creditCard2);
        User user2 = User.builder()
                .age(21)
                .startDate(Instant.now())
                .endDate(Instant.now())
                .name("Frank")
                .surname("Sinatra")
                .creditCards(new ArrayList<>())
                .build();
        CreditCard creditCard3 = CreditCard.builder()
                .expMonth("08")
                .expYear("24")
                .cardNumber("1111 2222 3333 4444")
                .user(user2)
                .build();
        CreditCard creditCard4 = CreditCard.builder()
                .expMonth("11")
                .expYear("20")
                .cardNumber("1111 3333 2222 4444")
                .user(user2)
                .build();
        CreditCard creditCard5 = CreditCard.builder()
                .expMonth("06")
                .expYear("22")
                .cardNumber("6262 6564 9548 6215")
                .user(user2)
                .build();
        user.getCreditCards().add(creditCard3);
        user.getCreditCards().add(creditCard4);
        user.getCreditCards().add(creditCard5);

        UserRepository repository = context.getBean(UserRepository.class);
        repository.save(user);
        repository.save(user2);
        for (int i = 0; i < 20; i++) {
            repository.save(User.builder()
                    .age(23)
                    .startDate(Instant.now())
                    .endDate(Instant.now())
                    .name("Jack" + i)
                    .surname("Daniels" + i)
                    .creditCards(new ArrayList<>())
                    .build());
        }
    }
}
