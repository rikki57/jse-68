package ru.nlmk.study.jse68.model;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "USERS")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    private Long id;
    private String name;
    private String surname;
    private int age;
    private Instant startDate;
    private Instant endDate;
    @Version
    private long version;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<CreditCard> creditCards;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", version=" + version +
                ", creditCards=" + creditCards +
                '}';
    }
}
