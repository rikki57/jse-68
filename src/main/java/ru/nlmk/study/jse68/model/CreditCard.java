package ru.nlmk.study.jse68.model;

import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "CREDIT_CARD")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ccard_id_seq")
    private Long id;

    @Column(name = "card_number")
    private String cardNumber;

    private String expMonth;

    private String expYear;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "userlink")
    private User user;
}
